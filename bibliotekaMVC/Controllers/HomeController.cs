﻿using bibliotekaMVC.Models;
using Library.Common;
using Library.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace bibliotekaMVC.Controllers
{
    public class HomeController : Controller
    {
        private UserService service;

        public HomeController()
        {
            service = new UserService();
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(service.GetUsers());
        }
        
        [HttpGet]
        public ActionResult AddUser()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddUser(AddUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                service.AddUser(model);
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult DeleteConfirmed(int id)
        {
            service.DeleteUser(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult EditUser(int id)
        {
            var user = service.EditUser(id);
            if (user == null) return HttpNotFound();
            return View(user); ;
        }

        [HttpPost]
        public ActionResult EditUser(EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                service.EditUser(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            return View(service.Details(id));
        }

    }
}

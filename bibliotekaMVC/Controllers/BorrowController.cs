﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Common;
using Library.DataAccess;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace bibliotekaMVC.Controllers
{
    public class BorrowController : Controller
    {
        private BorrowService service;

        public BorrowController()
        {
            service = new BorrowService();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetBorrows([DataSourceRequest] DataSourceRequest request)
        {
            var borrows = this.service.GetBorrows();
            return Json(borrows.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReturnBook(int id)
        {
            service.ReturnBook(id);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult ReturnBooks([DataSourceRequest] DataSourceRequest request, int[] data)
        {
            if (data != null)
            {
                service.ReturnBooks(data);
                return Json(new { value = "OK" });
            }
            return Json(new { value = "" });
        }

        public ActionResult AddBorrow()
        {
            return View();
        }


        public ActionResult GetUsers()
        {
            return Json(service.GetUsers(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBooks()
        {
            return Json(service.GetBooks(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetUsersGrid([DataSourceRequest] DataSourceRequest request)
        {
            var users = this.service.GetUsers();
            return Json(users.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UserBorrow()
        {
            return View();
        }
        
        public ActionResult DetailsUserBorrow(int id)
        {
            return View(service.GetUserBorrows(id));
        }

        

    }
}

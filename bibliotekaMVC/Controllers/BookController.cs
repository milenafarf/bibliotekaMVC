﻿using Library.Common;
using Library.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace bibliotekaMVC.Controllers
{
    public class BookController : Controller
    {
        private BookService service;

        public BookController()
        {
            service = new BookService();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddBook()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetBooks([DataSourceRequest] DataSourceRequest request)
        {
            var books = this.service.GetBooks();
            return Json(books.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddBook([DataSourceRequest] DataSourceRequest request, AddBookViewModel model)
        {
            if (ModelState.IsValid)
            {
                service.AddBook(model);
                return Json(new { value = "OK" });
            }
            else return View(model);
        }

        public ActionResult EditBook([DataSourceRequest] DataSourceRequest request, int id)
        {
            return PartialView(service.GetBook(id));
        }

        [HttpPost]
        public ActionResult EditBook([DataSourceRequest] DataSourceRequest request, EditBookViewModel model)
        {
            if (ModelState.IsValid)
            {
                service.EditBook(model);
                return Json(new { value = "OK" });
            }
            else return View(model);
        }

        public ActionResult GetBookGenres()
        {
            return Json(service.GetBookGenres(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details([DataSourceRequest] DataSourceRequest request, int id)
        {
            return View(service.Details(id));
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library.Common
{
    public class UserViewModel
    {
        public int UserId { get; set; }

        [Display(Name = "Imię")]
        public string FirstName { get; set; }

        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Data urodzin")]
        public System.DateTime BirthDate { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Nr telefonu")]
        public string Phone { get; set; }
               
        [DataType(DataType.Date)]
        [Display(Name = "Data dodania")]
        public System.DateTime AddDate { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Data edycji")]
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        [Display(Name = "Wypożyczone książki")]
        public int BooksBorrowed { get; set; }

        [Display(Name = "IsActive")]
        public string IsActive { get; set; }
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Common
{
    public class DetailsBookBorrowViewModel
    {
        public System.DateTime FromDate { get; set; }
        public System.DateTime ToDate { get; set; }
        public bool IsReturned { get; set; }
        public string userFN { get; set;  }
        public string userLN { get; set; }
    }
}

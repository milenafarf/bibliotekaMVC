﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Common
{
    public class EditBookViewModel
    {
        public int BookId { get; set; }

        [Required(ErrorMessage = "Wpisz autora")]
        [Display(Name = "Autor")]
        public string Author { get; set; }

        [Display(Name = "Tytuł")]
        [Required(ErrorMessage = "Wpisz tytuł")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Wpisz datę wydania")]
        [DataType(DataType.Date)]
        [Display(Name = "Data wydania")]
        public Nullable<System.DateTime> ReleaseDate { get; set; }

        [Required(ErrorMessage = "Wpisz ISBN")]
        public string ISBN { get; set; }

        [Display(Name = "Gatunek")]
        [Required(ErrorMessage = "Wybierz gatunek")]
        public int BookGenreId { get; set; }

        [Display(Name = "Liczba książek")]
        [Required(ErrorMessage = "Wpisz liczbę książek")]
        [Range(1,Int32.MaxValue, ErrorMessage = "Niepoprawna liczba książek")]
        public int Count { get; set; }

        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Common.BorrowViewModels
{
    public class IsBookBorrowed
    {
        public int BookId { get; set; }
        public int Title { get; set; }
        public bool IsBorrowed { get; set; }
    }
}

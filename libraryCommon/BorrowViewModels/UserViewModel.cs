﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Common.BorrowViewModels
{
    public class UserViewModel
    {
        public int UserId { get; set; }
        public string User { get; set; } //Imie + nazwisko
    }
}

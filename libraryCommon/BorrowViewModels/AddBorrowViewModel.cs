﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Common.BorrowViewModels
{
    public class AddBorrowViewModel
    {
        public int UserId { get; set; }

        [Display(Name = "Użytkownik")]
        public string User { get; set; }

        List<int> ChosenBooks { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Library.Common.BorrowViewModels
{
    public class BorrowedViewModel
    {
        public int BorrowId { get; set; }
        [Display(Name = "Autor")]
        public string Author { get; set; }
        [Display(Name = "Tytuł")]
        public string Title{ get; set; }
        public int UserId { get; set; }
        public string User { get; set; }
        public System.DateTime FromDate { get; set; }
        public System.DateTime ToDate { get; set; }
    }
}

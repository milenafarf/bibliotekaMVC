﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Common
{
    public class DetailsViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public ICollection<DetailsBorrowViewModel> BorrowHistory { get; set; }
        public ICollection<DetailsBorrowViewModel> BorrowNotReturned { get; set; }

    }
}

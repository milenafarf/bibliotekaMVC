﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Common
{
    public class DetailsBorrowViewModel
    {
        public string Title { get; set; }
        public System.DateTime FromDate { get; set; }
        public System.DateTime ToDate { get; set; }
        public bool IsReturned { get; set; }
        public ICollection<DetailsBorrowViewModel> BorrowHistory { get; set; }
    }
}

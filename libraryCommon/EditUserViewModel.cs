﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library.Common
{
    public class EditUserViewModel
    {
        public int UserId { get; set; }

        [Required(ErrorMessage = "Wpisz imię")]
        [Display(Name = "Imię")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Wpisz nazwisko")]
        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Wpisz datę urodzin")]
        [DataType(DataType.Date)]
        [Display(Name = "Data urodzin")]
        public System.DateTime BirthDate { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        
        [Required(ErrorMessage = "Wpisz email")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Nr telefonu")]
        public string Phone { get; set; }

        [Display(Name = "IsActive")]
        public bool IsActive { get; set; }
    }
}
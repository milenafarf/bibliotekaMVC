﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library.Common
{
    public class BookViewModel
    {
        [Required]
        [Display(Name = "Autor")]
        public string Author { get; set; }

        [Required]
        [Display(Name = "Tytuł")]
        public string Title { get; set; }

        public Nullable<System.DateTime> ReleaseDate { get; set; }

        [Required]
        public string ISBN { get; set; }

        [Required]
        public int BookGenreId { get; set; }

        [Required]
        public int Count { get; set; }

        [Required]
        public System.DateTime AddDate { get; set; }

        public Nullable<System.DateTime> ModifiedDate { get; set; }

        //public virtual DictBookGenre DictBookGenre { get; set; }
        //public virtual Borrow Borrow { get; set; }
    }
}
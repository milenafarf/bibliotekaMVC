﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library.Common
{
    public class AddUserViewModel
    {
        [Required(ErrorMessage = "Wpisz imię")]
        [Display(Name = "Imię")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Wpisz nazwisko")]
        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Wpisz datę urodzin")]
        [DataType(DataType.Date)]
        [Display(Name="Data urodzin")]
        public System.DateTime BirthDate { get; set; }

        [Required(ErrorMessage = "Wpisz email")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Nr telefonu")]
        public string Phone { get; set; }

    }
}
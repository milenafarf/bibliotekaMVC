﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Common
{
    public class BookDetailsVM
    {
        public int BookId { get; set; }
        [Display(Name = "Autor")]
        public string Author { get; set; }
        [Display(Name = "Tytuł")]
        public string Title { get; set; }
        [Display(Name = "data wydania")]
        public Nullable<System.DateTime> ReleaseDate { get; set; }
        public string ISBN { get; set; }
        public int BookGenreId { get; set; }
        [Display(Name = "Liczba książek")]
        public int Count { get; set; }
        [Display(Name = "Data dodania")]
        public System.DateTime AddDate { get; set; }
        [Display(Name = "Data modyfikacji")]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [Display(Name = "Historia wypożyczeń")]
        public IEnumerable<DetailsBookBorrowViewModel> history { get; set; }
    }
}

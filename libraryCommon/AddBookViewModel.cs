﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Common
{
    public class AddBookViewModel
    {
        [Required(ErrorMessage = "Wpisz Autora")]
        [Display(Name = "Autor")]
        public string Author { get; set; }

        [Required(ErrorMessage = "Wpisz tytuł")]
        [Display(Name = "Tytuł")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Wpisz datę wydania")]
        [DataType(DataType.Date)]
        [Display(Name = "Data wydania")]
        public Nullable<System.DateTime> ReleaseDate { get; set; }

        [Required(ErrorMessage = "Wpisz ISBN")]
        public string ISBN { get; set; }

        [Required(ErrorMessage = "Wybierz gatunek")]
        public int BookGenreId { get; set; }

        public string BookGenre { get; set; }

        [Required(ErrorMessage = "Wpisz ilość książek")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Niepoprawna liczba książek")]
        public int Count { get; set; }

        public System.DateTime AddDate { get; set; }

        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}

﻿using data;
using Library.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Library.DataAccess
{
    public class BookService
    {
        private BibliotekaEntities db;

        public BookService()
        {
            db = new BibliotekaEntities();
        }

        public List<GetBooksViewModel> GetBooks()
        {
            var result = (from b in db.Book
                          join bg in db.DictBookGenre on b.BookGenreId equals bg.BookGenreId
                          select new GetBooksViewModel()
                          {
                              BookId = b.BookId,
                              Author = b.Author,
                              Title = b.Title,
                              ReleaseDate = b.ReleaseDate,
                              ISBN = b.ISBN,
                              BookGenreId = b.BookId,
                              BookGenre = bg.Name,
                              Count = b.Count
                          }).ToList();

            return result;
        }

        public EditBookViewModel GetBook(int id)
        {
            var result = db.Book.Where(s => s.BookId == id).Select(s => new EditBookViewModel
            {
                BookId = s.BookId,
                Author = s.Author,
                Title = s.Title,
                ReleaseDate = s.ReleaseDate,
                ISBN = s.ISBN,
                BookGenreId = s.BookGenreId,
                Count = s.Count,
                ModifiedDate = s.ModifiedDate
            }).FirstOrDefault();

            return result;
        }

        public void AddBook(AddBookViewModel model)
        {
            Book book = new Book();
            book.Author = model.Author;
            book.Title = model.Title;
            book.ReleaseDate = model.ReleaseDate;
            book.ISBN = model.ISBN;
            book.BookGenreId = model.BookGenreId;
            book.Count = model.Count;
            book.AddDate = DateTime.Now;
            book.ModifiedDate = DateTime.Now;

            db.Book.Add(book);
            db.SaveChanges();

        }

        public bool EditBook(EditBookViewModel model)
        {
            Book book = db.Book.FirstOrDefault(b => b.BookId == model.BookId);

            if (book == null)
                return false;

            if (book != null)
            {
                book.Author = model.Author;
                book.Title = model.Title;
                book.ReleaseDate = model.ReleaseDate;
                book.ISBN = model.ISBN;
                book.BookGenreId = model.BookGenreId;
                book.Count = model.Count;
                book.ModifiedDate = DateTime.Now;

                db.SaveChanges();
            }
            return true;
        }

        public IEnumerable<DictBookGenreViewModel> GetBookGenres()
        {
            var result = (from g in db.DictBookGenre
                          select new DictBookGenreViewModel()
                              {
                                  BookGenreId = g.BookGenreId,
                                  Name = g.Name
                              });

            return result; 
        }

        public BookDetailsVM Details(int id)
        {
            Book book = db.Book.FirstOrDefault(b => b.BookId == id);

            var result = (from b in db.Book
                          join br in db.Borrow.DefaultIfEmpty() on b.BookId equals br.BookId
                          join u in db.User on br.UserId equals u.UserId
                          where b.BookId == id
                          select new DetailsBookBorrowViewModel()
                          {
                              FromDate = br.FromDate,
                              ToDate = br.ToDate,
                              IsReturned = br.IsReturned,
                              userFN = u.FirstName,
                              userLN = u.LastName
                          }).ToList();

            BookDetailsVM details = new BookDetailsVM();
            details.Title = book.Title;
            details.Author = book.Author;
            details.BookGenreId = book.BookGenreId;
            details.AddDate = book.AddDate;
            details.ModifiedDate = book.ModifiedDate;
            details.ISBN = book.ISBN;
            details.Count = book.Count;
            details.ReleaseDate = book.ReleaseDate;
            details.history = result;
            
            return details;
        }

    }
}
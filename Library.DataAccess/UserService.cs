﻿using data;
using Library.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Library.DataAccess
{
    public class UserService
    {
        private BibliotekaEntities db;

        public UserService()
        {
            db = new BibliotekaEntities();
        }

        public List<UserViewModel> GetUsers()
        {
            return db.User.Select(s => new UserViewModel
            {
                UserId = s.UserId,
                AddDate = s.AddDate,
                BirthDate = s.BirthDate,
                Email = s.Email,
                FirstName = s.FirstName,
                IsActive = s.IsActive ? "Yes" : "No",
                LastName = s.LastName,
                ModifiedDate = s.ModifiedDate,
                Phone = s.Phone,
                BooksBorrowed = s.Borrow.Where(c => c.IsReturned == false).Count()
            }).ToList();
        }

        public void AddUser(AddUserViewModel model)
        {
            User u = new User();
            u.FirstName = model.FirstName;
            u.LastName = model.LastName;
            u.BirthDate = model.BirthDate;
            u.Email = model.Email;
            u.Phone = model.Phone;
            u.AddDate = DateTime.Today;
            u.ModifiedDate = DateTime.Today;
            u.IsActive = true;

            db.User.Add(u);
            db.SaveChanges();

        }

        public EditUserViewModel EditUser(int id)
        {
            EditUserViewModel result;
            try
            {
                result = db.User.Where(s => s.UserId == id).Select(s => new EditUserViewModel
                {
                    UserId = s.UserId,
                    BirthDate = s.BirthDate,
                    Email = s.Email,
                    FirstName = s.FirstName,
                    IsActive = s.IsActive,
                    LastName = s.LastName,
                    ModifiedDate = s.ModifiedDate,
                    Phone = s.Phone
                }).Single();

                return result;
            }

            catch { return null; }
        }

        public void EditUser(EditUserViewModel model)
        {
            User user = db.User.FirstOrDefault(u => u.UserId == model.UserId);
            if (user != null)
            {
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.BirthDate = model.BirthDate;
                user.Email = model.Email;
                user.Phone = model.Phone;
                user.ModifiedDate = DateTime.Today;
                user.IsActive = model.IsActive;

                //db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void DeleteUser(int id)
        {
            User user = db.User.FirstOrDefault(u => u.UserId == id);
            if (user != null && user.Borrow.Count() == 0)
            {
                user.IsActive = false;
                db.SaveChanges();
            }
        }

        public DetailsViewModel Details(int id)
        {
            User user = db.User.FirstOrDefault(u => u.UserId == id);
            if (user == null)
                return null;

            var result = (from u in db.User
                          join br in db.Borrow on u.UserId equals br.UserId
                          join b in db.Book on br.BookId equals b.BookId
                          where u.UserId == id
                          select new DetailsBorrowViewModel()
                          {
                              Title = b.Title,
                              FromDate = br.FromDate,
                              ToDate = br.ToDate,
                              IsReturned = br.IsReturned
                          }).ToList();

            List<DetailsBorrowViewModel> hist = new List<DetailsBorrowViewModel>();
            List<DetailsBorrowViewModel> notReturned = new List<DetailsBorrowViewModel>();

            foreach (var item in result)
            {
                if (item.IsReturned) hist.Add(item);
                else notReturned.Add(item);
            }

            DetailsViewModel detailsVM = new DetailsViewModel()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                BorrowHistory = hist,
                BorrowNotReturned = notReturned
            };

            return detailsVM;
        }
    }
}
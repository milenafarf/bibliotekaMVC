﻿using data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Library.Common.BorrowViewModels;

namespace Library.DataAccess
{
    public class BorrowService
    {
        private BibliotekaEntities db;

        public BorrowService()
        {
            db = new BibliotekaEntities();
        }

        public List<BorrowedViewModel> GetBorrows()
        {
            var result = (from b in db.Book
                          join br in db.Borrow on b.BookId equals br.BookId
                          join u in db.User on br.UserId equals u.UserId
                          where br.IsReturned == false //tylko nieoddane książki
                          select new BorrowedViewModel()
                          {
                              BorrowId = br.BorrowId,
                              Author = b.Author,
                              Title = b.Title,
                              UserId = u.UserId,
                              User = u.FirstName + " " + u.LastName,
                              FromDate = br.FromDate,
                              ToDate = br.ToDate
                          }).ToList();

            return result;
        }

        public void ReturnBook(int id)
        {
            Borrow borrow = db.Borrow.FirstOrDefault(b => b.BorrowId == id);
            borrow.IsReturned = true;
            db.SaveChanges();
        }

        public void ReturnBooks(int[] data)
        {
            var borrow = from br in db.Borrow
                         where data.Contains(br.BorrowId)
                         select br;

            foreach (var br in borrow)
            {
                br.IsReturned = true;
            }

            db.SaveChanges();
        }

        public List<BookViewModel> GetBooks()
        {
            var result = db.Book.Where(s => s.Count > db.Borrow.Where(br => br.BookId == s.BookId).Count(c => c.IsReturned == false)).
                Select(ss => new BookViewModel()
                          {
                              BookId = ss.BookId,
                              Title = ss.Title
                          }).ToList();

            return result;
        }

        public List<UserViewModel> GetUsers()
        {
            var result = (from u in db.User
                          where u.Borrow.Any()
                          select new UserViewModel()
                          {
                              UserId = u.UserId,
                              User = u.FirstName + " " + u.LastName
                          }).ToList();

            return result;
        }

        //wypozyczone ksiazki jendego uzytkonika o podanym id
        public List<BorrowedViewModel> GetUserBorrows(int id)
        {
            var result = (from b in db.Book
                          join br in db.Borrow on b.BookId equals br.BookId
                          where br.IsReturned == false //tylko nieoddane książki
                          && br.UserId == id
                          select new BorrowedViewModel()
                          {
                              BorrowId = br.BorrowId,
                              Author = b.Author,
                              Title = b.Title,
                              FromDate = br.FromDate,
                              ToDate = br.ToDate
                          }).ToList();
            return result;
        }

    }
}
